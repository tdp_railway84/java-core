import java.util.Scanner;

public class Exercise1 {
    public static void main(String[] args) {
        Exercise1 ex1 = new Exercise1();
        ex1.Que1_1();
    }

    public void Que1_1(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Mời nhập vào lương 1: ");
        float luong1 = sc.nextFloat();
        System.out.println("Mời nhập vào lương 2: ");
        float luong2 = sc.nextFloat();

        int luong1convert = (int)luong1;
        int luong2convert = (int)luong2;
        System.out.println("Lương 1 khi làm tròn: " + luong1convert);
        System.out.println("Lương 2 khi làm tròn: " + luong2convert);
    }
}
