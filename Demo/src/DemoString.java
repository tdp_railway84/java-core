public class DemoString {
    public static void main(String[] args) {
//        String a = "a";
//        String b = "a";
//        String c = new String("a");
//        String d = new String("a");
//        System.out.println(a.equals(c));

        //method equals bên trong String có tác dụng so sánh 2 giá trị String với nhau
        //== so sánh địa chỉ
        // method equalsInoreCase So sánh 2 giá trị không phân biệt hoa hay thường


//        String s = "Java core";
//        System.out.println(s.toUpperCase());
//        System.out.println(s.toLowerCase());
//        System.out.println(s);

//        String s = "   Java core    ";
//        System.out.println(s);
//        System.out.println(s.trim());

        //charAt() //trả về giá trị tại vị trí index được gọi
        String s = "Java core";
        char c = s.charAt(2);
        System.out.println(c);

        //subString tách chuỗi
        //replace thay thế
        //split: tách chuỗi dựa trên khoảng trắng
        //contains: kiểm tra sự tổn tại của chuỗi

    }
}
