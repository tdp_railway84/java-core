class Animal {
    public void eat() {
        System.out.println("Eating…");
    }
}
class Dog extends Animal {
    public void bark() {
        System.out.println("Barking…");
    }
}
public class DemoTest {
/*    public static void main(String[] args) {
        Dog myDog = new Animal();
        myDog.makeSound();
    }
}
class Animal{
    void makeSound(){
        System.out.println("Animal is making a sound");
    }
}
class Dog extends Animal{
    void makeSound(){
        System.out.println("Dog is barking");
    }*/

    public static void main(String[] args) {

        Dog d = new Dog();

        d.eat(); // gọi phương thức từ lớp cha

        d.bark(); // gọi phương thức từ lớp con

    }

}
