import java.util.Scanner;

public class DemoInput {
    public static void main(String[] args) {
        //Muốn nhập vào từ màn hình console các giá trị để gán vào 1 biến trong java
        Scanner scanner = new Scanner(System.in);
        System.out.print("Mời bạn nhập vào số bất kỳ: ");
        int number = scanner.nextInt();  //giá trị được nhập từ console
        System.out.println("Số vừa nhập vào là: " + number);
    }
}
