public class DemoDataCasting {
    public static void main(String[] args) {
//        int a = 10;
//        long b = a; //ép kiểu dữ liệu của a thành long

        long a = 10;
        int b = (int)a; // ép kiểu dữ liệu của a nhỏ hơn dữ liệu ban đầu
    }
}
