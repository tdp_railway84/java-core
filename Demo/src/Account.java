import java.time.LocalDate;

public class Account {
    int accountId;
    String email;
    String userName;
    String fullName;
    LocalDate createDate;

    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                ", email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", createDate=" + createDate +
                '}';
    }
}
