import java.io.File;
import java.io.IOException;

public class DemoCreateFile {
    public static void main(String[] args) throws IOException {
//        File file = new File("D:\\Tdp_Railway84");
//        file.createNewFile();// Khi gọi tới method này, tiềm ẩn ngoại lệ: đường dẫn không tồn tại
        // nên method đó đang khai báo có ngoại lệ xảy ra, cần phải giải quyết
        // Có 2 cách giải quyết:
        //C1- Lại ném ngoại lệ này ra ngoài method hiện tại, để các nơi khác gọi tới method hiện tại này phải giải quyết ngoại lệ đó
        try {
            DemoCreateFile demoCreateFile = new DemoCreateFile();
            demoCreateFile.test();
            //Vid dụ trong khối này sinh ra nhiều ngoại lệ cần xử lý
            //Thì có thể catch nhiều ngoại lệ 1 lúc
            //Khi bắt ngoại lệ thuộc Checked exception thì cần method đó ném ra ngoại lệ tương ứng
        } catch (IOException e) {
            //IOException: Là ngoại lệ liên quan đến file
            //SQLSyntaxException: là ngoại lệ liên quan đến cú pháp
//            ... Đây là những ngoại lệ checked Exception phải giải quyết
            // Khi có lỗi sẽ in ra thông tin lỗi
            System.out.println("Có lỗi xảy ra ");
            System.out.println(e.getMessage());
        }
        System.out.println("Code tiếp...");
    }

    public void test() throws IOException {
        File file = new File("D:/Tdp_Railway84/baitap.doc");
        file.createNewFile();
    }
}
