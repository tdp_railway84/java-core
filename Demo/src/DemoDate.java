import java.text.SimpleDateFormat;
import java.util.Date;

public class DemoDate {
    public static void main(String[] args) {
        Date date1 = new Date();
        System.out.println(date1);
        String pattern = "dd/MM/yyyy hh:mm:ss"; //tạo định dạng muốn in gán cho biến pattern
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern); //tạo biến simpledateformat bắt định dạng pattern
        String date = simpleDateFormat.format(date1); // Format kiểu String cho date
        System.out.println(date);
    }
}
