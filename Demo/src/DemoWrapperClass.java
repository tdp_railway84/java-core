public class DemoWrapperClass {
    public static void main(String[] args) {
        boolean a = true; // giá trị mặc định là false

        String ab = "true";

        Boolean b = Boolean.parseBoolean(ab); // giá trị mặc định là null

        Boolean c = Boolean.valueOf(ab); //--> gọi là boxing, từ nguyên thủy sang object

        Boolean a2 = true; // a2 có kiểu dữ liệu là object boolean
        boolean b2 = a2; //C1: ép kiểu a 2 từ Object --> nguyên thủy, Unboxing
        System.out.println(b);
    }
}
