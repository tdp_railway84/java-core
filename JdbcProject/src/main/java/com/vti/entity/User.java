package com.vti.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor// Tạo hàm khởi tạo không tham số
@AllArgsConstructor//Tạo hàm khởi tạo có đầy đủ tham số
//Anotation để thay thế cho các phương thức trong bài toàn VD Getter, Setter, toString
public class User {
    private int id;
    private String email;
    private String userName;
    private String password;
    private String dateOfBirth;
    private Department department;
    private Role role;

//    public int getAccountid() {
//        return accountid;
//    }
//
//    public void setAccountid(int accountid) {
//        this.accountid = accountid;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public String getRole() {
//        return role;
//    }
//
//    public void setRole(String role) {
//        this.role = role;
//    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", departmentName=" + department.getDepartmentName() +
                ", role=" + role +
                '}';
    }
}
