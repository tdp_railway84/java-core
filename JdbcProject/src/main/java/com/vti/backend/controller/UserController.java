package com.vti.backend.controller;

import com.vti.backend.service.UserService;
import com.vti.backend.service.UserServiceImpl;
import com.vti.entity.User;

import java.io.IOException;
import java.util.List;

public class UserController {
    private UserService userService = new UserServiceImpl();

    public List<User> getAll() {
        return userService.getAll();
    }

    public User login(String user, String password) throws IOException {
        return userService.Login(user, password);
    }

    public User getById(int id) throws IOException {
        return userService.getByID(id);
    }
//    public int getMenu(Role role) {
//        return accountService.GetMenuUser(role);
//    }

    public User getByWord(String keyword) throws IOException {
        return userService.findbyWord(keyword);
    }

    public List<User> findAllUser() throws IOException {
        return userService.findAllUser();
    }

    public void AddUser(String userName, String email, String dateOfBirth) throws IOException {
        userService.AddUser(userName, email, dateOfBirth);
    }
}
