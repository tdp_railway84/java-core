package com.vti.backend.service;

import com.vti.entity.Role;
import com.vti.entity.User;

import java.io.IOException;
import java.util.List;
//File Interface service sẽ liệt kê các chức năng có thể có của đối tượng Account
public interface UserService {
    List<User> getAll();

    // Chức năng lấy đối tượng theo ID
    User getByID(int id) throws IOException;

    // Chức năng login
    User Login(String user, String password) throws IOException;
//    int getMenu(Role role);
//    int GetMenuUser(Role role);
    User findbyWord(String keyword) throws IOException;

    List<User> findAllUser() throws IOException;

    void AddUser(String userName, String email, String dateOfBirth);
}
