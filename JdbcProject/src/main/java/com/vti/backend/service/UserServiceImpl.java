package com.vti.backend.service;

import com.vti.backend.repository.UserRepository;
import com.vti.backend.service.UserService;
import com.vti.entity.Role;
import com.vti.entity.User;

import java.io.IOException;
import java.util.List;

public class UserServiceImpl implements UserService {
    // Logic bên service tạm thời chưa viết gì
    // Coi đây là nơi trung gian truyền dữ liệu

    private UserRepository userRepository = new UserRepository();

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getByID(int id) throws IOException {
        return userRepository.findUserByID(id);
    }

    @Override
    public User Login(String user, String password)  {
        return  userRepository.login(user,password);
    }

    @Override
    public User findbyWord(String keyword) throws IOException {
        return userRepository.findByWord(keyword);
    }

    @Override
    public List<User> findAllUser() throws IOException {
        return userRepository.findAllUser();
    }

    @Override
    public void AddUser(String userName, String email, String dateOfBirth) {
        try {

            User user1 = userRepository.findByWord(userName);
            User user2 = userRepository.findByWord(email);
            if (user1 != null || user2 != null) {
                System.out.println("User name hoặc email đã tồn tại. Không được thêm mới");
                return;
            }
            userRepository.AddUser(userName,email,dateOfBirth);

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

//    @Override
//    public int getMenuUser(Role role) {
//        return userRepository.GetMenuUser(role);
//    }
//

}
