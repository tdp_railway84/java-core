package com.vti.backend.repository;

import com.vti.Utils.JdbcUtils;
import com.vti.Utils.ScannerUtils;
import com.vti.entity.Department;
import com.vti.entity.Role;
import com.vti.entity.User;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserRepository {
    public List<User> getAll() {
        List<User> userList = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Account";
            Connection connection = JdbcUtils.GetConnectionDB();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String email = resultSet.getString("email");
                String userName = resultSet.getString("user_name");
                User user = new User();
                user.setId(id);
                user.setEmail(email);
                user.setUserName(userName);
                userList.add(user);
            }
            System.out.println(userList);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return userList;
    }

    /*public void Login() {
        System.out.println("Mời bạn nhập vào tên User");
        String user = ScannerUtils.inputString();
        System.out.println("Mời bạn nhập Mật khẩu");
        String password = ScannerUtils.inputString();
        try {
            Connection connection = GetConnection.GetConnectionDB();
            String sql = "SELECT * FROM User\n" +
                    "WHERE userName = ? AND password = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user);
            preparedStatement.setString(2, password);

            ResultSet resultSet = preparedStatement.executeQuery(sql);

            if (resultSet.next()) { //So sánh user nhập vào với Data trong DB
                System.out.println("Đăng nhập thành công");
                if (user == "USER"){

                }
            } else {
                System.out.println("Đăng nhập không thành công, vui lòng kiểm tra User và Password");
            }
            // Nếu role là USER sẽ getMenuByUser
            // Nếu role là ADMIN sẽ getMenyByAdmin
        } catch (Exception e) {
            System.err.println(e.getMessage());
            ;
        }
    }*/

    public User login(String username, String password) {
        User user = null;
        try {
            String sql = "SELECT * FROM User WHERE user_name = ? AND password = ?";
            Connection connection = JdbcUtils.GetConnectionDB();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            //Set các giá trị vào biến trong câu lệnh sql
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);

            //Excute câu lệnh
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String roleString = resultSet.getString("role");
                Role roleEnum = Role.valueOf(roleString);
                user = new User();
                user.setRole(roleEnum);
                //Có thông tin trả về, lấy các thông tin đó gán vào đối tượng User
            } else {
                //Sai
                /*          System.out.println("User hoặc Password chưa đúng, mời nhập lại");*/
            }

            //Kết nối với db để set các giá trị account để cuối cùng return ra giá trị đó
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return user;
    }

    public int GetMenuUser(Role role) {
        if (role == Role.ADMIN) {
            System.out.println("Menu dành cho ADMIN");
            System.out.println("Mời chọn chức năng sử dụng: " + "\n"
                    + "1- Hiển thị thông tin User " + "\n"
                    + "2- Xóa User theo ID " + "\n"
                    + "3- Thay đổi mật khẩu của 1 User " + "\n"
                    + "4- Thêm mới 1 User " + "\n" // Mật khẩu mặc định 123456, role USER
                    + "5- Hiển thị tất cả các Department " + "\n" // Hiển thị theo dạng bảng
                    + "6- Xóa Department theo ID " + "\n"
                    + "7- Thay đổi tên 1 Department " + "\n"
                    + "8- Thêm mới 1 Department " + "\n"
                    + "9- Hiển thị thông tin User " + "\n");
            int x = ScannerUtils.inputNumber("Mời bạn nhập lại số từ 1 đến 9");
            return x;
        } else {
            System.out.println("Menu dành cho USER");
            System.out.println("Tính năng của hệ thống: " + "\n"
                    + "1- Hiển thị thông tin User " + "\n"
                    + "2- Tìm kiếm User theo ID " + "\n"
                    + "3- Tìm kiếm User theo username và email " + "\n"
                    + "4- Hiển thị tất cả các Department " + "\n" // Hiển thị theo dạng bảng
                    + "5- Tìm kiếm Department theo ID " + "\n"
                    + "7- Tìm kiếm Department theo Department Name " + "\n"
                    + "8- Hiển thị thông tin User " + "\n");
            int y = ScannerUtils.inputNumber("Mời bạn nhập lại số từ 1 đến 8");
            return y;
        }
    }

    public User findUserByID(int Id) throws IOException {
        try {
            String sql = "select * from user where id = ?";
            Connection connection = JdbcUtils.GetConnectionDB();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, Id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String roleString = resultSet.getString("role");
                Role roleEnum = Role.valueOf(roleString);
                User user = new User();
                user.setRole(roleEnum);
                user.setUserName(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                return user;
            }
        } catch (IOException | SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.CloseConnection();
        }
        return null;
    }

    public User findByWord(String keyword) throws IOException {
        try {
            String sql = "select * from User where user_name = ? or email =?";
            Connection connection = JdbcUtils.GetConnectionDB();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, keyword);
            preparedStatement.setString(2, keyword);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String roleString = resultSet.getString("role");
                Role roleEnum = Role.valueOf(roleString);
                User user = new User();
                user.setRole(roleEnum);
                user.setUserName(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                return user;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JdbcUtils.CloseConnection();
        }
        return null;
    }


    public List<User> findAllUser() throws IOException {
        List<User> userList = new ArrayList<>();
        try {
            String sql = "select U.*, D.department_name from user U\n" +
                    "inner join department D on U.department_id = D.department_id";
            Connection connection = JdbcUtils.GetConnectionDB();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                String roleString = resultSet.getString("role");
                Role roleEnum = Role.valueOf(roleString);
                User user = new User();
                user.setRole(roleEnum);
                user.setUserName(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));

                Department department = new Department();
                department.setId(resultSet.getInt("department_id"));
                department.setDepartmentName(resultSet.getString("department_name"));

                user.setDepartment(department);
                userList.add(user);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.CloseConnection();
        }
        return userList;
    }

    public void AddUser(String userName, String email, String dateOfBirth) throws IOException {
        try {
            String sql = "INSERT INTO user (user_name, email, date_of_birth, password,role )" +
                    "VALUES(?,?,?,?,?);";
            Connection connection = JdbcUtils.GetConnectionDB();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, email);
            String patten = "yyyy-mm-dd";
            SimpleDateFormat format = new SimpleDateFormat(patten);
            Date date = format.parse(dateOfBirth);
            preparedStatement.setDate(3, new java.sql.Date(date.getTime()));
            preparedStatement.setString(4,"123456");
            preparedStatement.setString(5,"USER");


            int number = preparedStatement.executeUpdate();
            if (number == 0) {
                System.out.println("Thêm mới thất bại");
            } else {
                System.out.println("Thêm mới thành công");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            JdbcUtils.CloseConnection();
        }

    }
}
