package com.vti.Utils;

public class GetMenu {
    public int GetMenuByAdmin(int x) {
        System.out.println("Tính năng của hệ thống: " + "\n"
                + "1- Hiển thị thông tin User " + "\n"
                + "2- Xóa User theo ID " + "\n"
                + "3- Thay đổi mật khẩu của 1 User " + "\n"
                + "4- Thêm mới 1 User " + "\n" // Mật khẩu mặc định 123456, role USER
                + "5- Hiển thị tất cả các Department " + "\n" // Hiển thị theo dạng bảng
                + "6- Xóa Department theo ID " + "\n"
                + "7- Thay đổi tên 1 Department " + "\n"
                + "8- Thêm mới 1 Department " + "\n"
                + "9- Hiển thị thông tin User " + "\n");
        x = ScannerUtils.inputNumber("Mời bạn nhập lại số từ 1 đến 9");
        return x;
    }

    public int GetMenuByUser(int x) {
        System.out.println("Tính năng của hệ thống: " + "\n"
                + "1- Hiển thị thông tin User " + "\n"
                + "2- Tìm kiếm User theo ID " + "\n"
                + "3- Tìm kiếm User theo username và email " + "\n"
                + "4- Hiển thị tất cả các Department " + "\n" // Hiển thị theo dạng bảng
                + "5- Tìm kiếm Department theo ID " + "\n"
                + "7- Tìm kiếm Department theo Department Name " + "\n"
                + "8- Hiển thị thông tin User " + "\n");
        x = ScannerUtils.inputNumber("Mời bạn nhập lại số từ 1 đến 8");
        return x;
    }
}
