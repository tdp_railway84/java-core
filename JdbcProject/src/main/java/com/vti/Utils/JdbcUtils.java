package com.vti.Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class JdbcUtils {
    static Connection connection = null;

    public static Connection GetConnectionDB() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("JdbcProject/src/main/resources/db.properties"));

        String userName = properties.getProperty("user");
        String password = properties.getProperty("pass");
        String url = properties.getProperty("url");
        String driver = properties.getProperty("driver");
        
        try {
            Class.forName(driver); //checked
            connection = DriverManager.getConnection(url, userName, password);
            if (connection != null) {
                System.out.println("Kết nối với MySQL thành công");
            } else {
                System.out.println("Kết nối thất bại");
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
        return connection;
    }

    public static Connection CloseConnection() throws IOException {
        if (connection != null) {
            try {
                connection.close();
                System.out.println("Closed Connection");
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        return connection;
    }
}
