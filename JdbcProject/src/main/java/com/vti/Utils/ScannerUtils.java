package com.vti.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ScannerUtils {
    //Nhập số
    static Scanner sc = new Scanner(System.in);

    public static int inputNumber(String errorMessenger) {
        while (true) {
            try {
                return Integer.parseInt(sc.nextLine());
            } catch (Exception e) {
                System.out.println(errorMessenger);
            }
        }
    }

    public static float inputFloatNumber(float min, float max) {
        float number = sc.nextFloat();
        while (number < min || number > max) {
            System.out.println("Giá trị không thỏa mãn, mời bạn nhập lại");
            number = sc.nextFloat();
        }
        return number;
    }

    public static int inputNumberByminandmax(int min, int max) {
        int number = sc.nextInt();
        while (number < min || number > max) {
            System.out.println("Giá trị không thỏa mãn, mời bạn nhập lại");
            number = sc.nextInt();
        }
        return number;
    }

    //Nhập số điện thoại
    public static int inputPhoneNumber(String errorMessenger) {
        while (true) {
            try {
                String number = sc.nextLine();
                if (number.length() > 12) {
                    System.out.println("Số điện thoại bao gồm 12 số, Mời bạn nhập lại");
                    number = sc.nextLine();
                } else
                    return Integer.parseInt(number);
                ;
            } catch (Exception e) {
                System.out.println(errorMessenger);
            }
        }
    }


    //Nhập chuỗi
    public static String inputString() {
        while (true) {
            String input = sc.nextLine();
            if (input.isEmpty()) {
                System.out.println("Mời bạn nhập lại");
                continue;
            } else {
                return input;
            }
        }
    }

    //Nhập email

    public static String inputEmail() {
        while (true) {
            String input = sc.nextLine();
            if (input.isEmpty()) {
                System.out.println("Mời bạn nhập lại");
                continue;
            } else if (!input.contains("@")) {
                System.out.println("Email phải có kí tự @, xin mời nhập lại");
            } else {
                return input;
            }
        }
    }

    //Nhập ngày tháng

    public static Date inputDate() throws ParseException {
        Date date = null;
        String datePattern = "\\d{1,2}[-|/]\\d{1,2}[-|/]\\d{4}";
        String inputDate = sc.nextLine();
        try {
            while (!Pattern.matches(inputDate, datePattern)) {
                System.out.println("Mời bạn nhập lại theo form dd/mm/yyyy");
                sc.nextLine();
            }
            date = new SimpleDateFormat("dd/mm/yyyy").parse(inputDate);
            return date;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return date;
    }

    public static int inputNumberByMinMax(int min, int max) {
        int number = sc.nextInt();
        while (number < min || max < number) {
            System.out.println("Số nhập vào phải trong khoảng từ " + min + " đến " + max + ". Mời nhập lại");
            number = sc.nextInt();
        }
        return number;
    }

    public static String UserName() {
        while (true) {
            String input = sc.nextLine();
            if (input.isEmpty()) {
                System.out.println("Mời bạn nhập lại");
                continue;
            } else {
                return input;
            }
        }
    }
}

