package com.vti.Utils;


import com.vti.entity.User;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DemoJdbcUtils {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
        //B1: Xác định câu truy vấn SQL (Có 2 kiểu câu truy vấn)
        // * Kiểu 1: Câu lệnh truy vấn tĩnh (Không có tham số truyền vào)
        // * Kiểu 2: Câu lệnh truy vấn động (Có tham số truyển vào)
        String sql = "SELECT * FROM Account";

        //B2: Tạo 1 Connection
        Connection connection = JdbcUtils.GetConnectionDB();

        //B3: Tạo ra loại câu lệnh
        // Nếu câu lệnh tĩnh thì tạo đối tượng Statement
        // Nếu là câu lệnh động thì tạo đối tượng preparedStatement
        Statement statement = connection.createStatement();

        //B4: Chạy excute và hứng kết quả
        ResultSet resultSet = statement.executeQuery(sql);

        //B5: Conver kết quả trả về sang đối tượng của Java
        //Vd lấy thông tin của dòng đầu tiên
        List<User> userList = new ArrayList<>();
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            String userName = resultSet.getString("user_name");

            User user = new User();
            user.setId(id);
            user.setUserName(userName);
            userList.add(user);
        }
        JdbcUtils.CloseConnection();
        System.out.println(userList);
    }

}

