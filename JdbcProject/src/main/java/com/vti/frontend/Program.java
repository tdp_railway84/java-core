package com.vti.frontend;

import com.vti.Utils.ScannerUtils;
import com.vti.entity.Role;
import com.vti.entity.User;

import java.io.IOException;
import java.text.ParseException;

public class Program {
    Function function = new Function();

    public static void main(String[] args) throws IOException {
        Program program = new Program();
        program.menuUser();
        System.out.println("---------------------------------");
        System.out.println("|  id  |");

    }


    public void menuLogin() throws IOException {
        //Gọi tới function login
        User user = function.login();
        if (user.getRole() == Role.ADMIN) {
            //điều hướng sang menu admin
            menuAdmin();

        } else {
            //điều hướng sang menu user
            menuUser();
        }
    }

    private void menuUser() throws IOException {
        while (true) {
            System.out.println("Menu dành cho USER");
            System.out.println("Tính năng của hệ thống: " + "\n"
                    + "1- Hiển thị thông tin User " + "\n"
                    + "2- Tìm kiếm User theo ID " + "\n"
                    + "3- Tìm kiếm User theo username và email " + "\n"
                    + "4- Thêm mới 1 user " + "\n" // pass = 123456, Role = User
                    + "5- Tìm kiếm Department theo ID " + "\n"
                    + "7- Tìm kiếm Department theo Department Name " + "\n"
                    + "8- Hiển thị thông tin User " + "\n"
                    + "9- Thoát chương trình " + "\n");
            int choose = ScannerUtils.inputNumberByminandmax(1, 10);

            switch (choose) {
                case 1:
                    function.findAllUser();
                    break;
                case 2:
                    function.findUserById();
                    break;
                case 3:
                    function.findByWord();
                    break;
                case 4:
                    try {
                        function.CreateUser();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    break;
                case 9:
                    return;
            }
        }

    }

    private void menuAdmin() {
    }
}
