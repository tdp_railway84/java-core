package com.vti.frontend;

import com.vti.Utils.ScannerUtils;
import com.vti.backend.controller.UserController;
import com.vti.entity.User;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class Function {
    UserController userController = new UserController();

    public User login() throws IOException {
        System.out.println("Mời bạn nhập vào user name");
        String userName = ScannerUtils.inputString();

        System.out.println("Mời bạn nhập vào password");
        String password = ScannerUtils.inputString();

        User user = userController.login(userName, password);
        while (user == null) {
            System.out.println("User hoặc Password không đúng, mời nhập lại");
            System.out.println("Mời bạn nhập vào user name");
            userName = ScannerUtils.inputString();
            System.out.println("Mời bạn nhập vào password");
            password = ScannerUtils.inputString();
            user = userController.login(userName, password);
        }
        return user;
    }

    public void findUserById() throws IOException {
        System.out.println("Mời bạn nhập vào Id muốn tìm kiếm");
        int id = ScannerUtils.inputNumber("Mời bạn nhập lại");
        User user = userController.getById(id);
        if(user == null){
            System.out.println("Không tồn tại user");
        } else
            System.out.println(user);
    }

    public void findAllUser() {
        System.out.println("Lấy ra tất cả User");
        List<User> userList = null;
        try {
            userList = userController.findAllUser();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(userList);
    }



    public void findByWord() {
        System.out.println("Mời nhập vào từ khóa tìm kiếm");
        String keyWord = ScannerUtils.inputString();
        User user = null;
        try {
            user = userController.getByWord(keyWord);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (user == null) {
            System.out.println("Không tồn tại");
        } else {
            System.out.println(user);
        }
    }

    public void CreateUser() throws ParseException, IOException {
        System.out.println("Nhập vào user name");
        String userName = ScannerUtils.inputString(); // Thay method chuyên để nhập UserName theo yêu cầu đề bài

        System.out.println("Mời nhập vào email");
        String email = ScannerUtils.inputString();

        System.out.println("Mời nhập vào ngày sinh");
        String dateString = String.valueOf(ScannerUtils.inputString());

        userController.AddUser(userName,email,dateString);
    }
}
