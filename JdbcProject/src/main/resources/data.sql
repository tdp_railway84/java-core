CREATE TABLE IF NOT EXISTS Department
(
    department_id   INT AUTO_INCREMENT PRIMARY KEY,
    department_name VARCHAR(50) NOT NULL UNIQUE
);
SET auto_increment_increment = 1;
INSERT INTO checkjdbc.department (department_name)
VALUES ('JAVA');
INSERT INTO checkjdbc.department (department_name)
VALUES ('PHP');
INSERT INTO checkjdbc.department (department_name)
VALUES ('SCRUM MASTER');
INSERT INTO checkjdbc.department (department_name)
VALUES ('SALE');
INSERT INTO checkjdbc.department (department_name)
VALUES ('PURCHASE');
ALTER TABLE department
    AUTO_INCREMENT = 1;
SELECT *
FROM department;
DELETE
FROM department;
SELECT LAST_INSERT_ID();
create table User
(
    id            int auto_increment primary key,
    role          enum ('ADMIN','USER') not null,
    user_name     nvarchar(50)          not null unique,
    password      nvarchar(50)          not null,
    email         varchar(30)           not null unique,
    date_of_birth date,
    department_id int,
    foreign key (department_id) REFERENCES department (department_id)
);

INSERT INTO checkjdbc.user (role, user_name, password, email, date_of_birth, department_id)
VALUES ('ADMIN', 'phuongtd', '182995', 'phuongg@gmail.com', '2024-02-21', 1)


