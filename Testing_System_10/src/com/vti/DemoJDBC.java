package com.vti;


import com.vti.entity.Account;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DemoJDBC {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //B1: Xác định câu truy vấn SQL (Có 2 kiểu câu truy vấn)
        // * Kiểu 1: Câu lệnh truy vấn tĩnh (Không có tham số truyền vào)
        // * Kiểu 2: Câu lệnh truy vấn động (Có tham số truyển vào)
        String sql = "SELECT * FROM Account";

        //B2: Tạo 1 Connection
        Connection connection = GetConnection.GetConnectionDB();

        //B3: Tạo ra loại câu lệnh
        // Nếu câu lệnh tĩnh thì tạo đối tượng Statement
        // Nếu là câu lệnh động thì tạo đối tượng preparedStatement
        Statement statement = connection.createStatement();

        //B4: Chạy excute và hứng kết quả
        ResultSet resultSet = statement.executeQuery(sql);

        //B5: Conver kết quả trả về sang đối tượng của Java
        //Vd lấy thông tin của dòng đầu tiên
        List<Account> accountList = new ArrayList<>();
        while (resultSet.next()){
            Integer id = resultSet.getInt("id");
            String userName = resultSet.getString("user_name");

            Account account = new Account();
            account.setAccountid(id);
            account.setUserName(userName);
            accountList.add(account);
        }
        System.out.println(accountList);

    }

}

