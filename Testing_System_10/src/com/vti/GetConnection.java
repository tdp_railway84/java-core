package com.vti;

import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.DriverManager;

public class GetConnection {
public static @Nullable
    Connection  GetConnectionDB() {
        Connection connection = null;
        String userName = "root";
        String password = "root";
        String url = "jdbc:mysql://localhost:3306/checkjdbc";
        String driver = "com.mysql.cj.jdbc.Driver";
        try {
            Class.forName(driver); //checked
            connection = DriverManager.getConnection(url, userName, password);
            if (connection != null) {
                System.out.println("Kết nối với MySQL thành công");
            } else {
                System.out.println("Kết nối thất bại");
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
        return connection;
    }
}
