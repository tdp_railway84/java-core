package com.vti.entity;

import com.vti.frontend.ScannerUtils;

public class CongNhan extends Canbo {

    private int capBac;

    public CongNhan(String hoTen, int tuoi, Gioitinh gioitinh, String diaChi, int capBac) {
        super(hoTen, tuoi, gioitinh, diaChi);
        this.capBac = capBac;
    }

    public int getCapBac() {
        return capBac;
    }

    public void setCapBac() {
        this.capBac = ScannerUtils.inputNumberByminandmax(1,10);
    }

    @Override
    public String toString() {
        return "CongNhan{" +
                "capBac=" + capBac +
                '}';
    }
}
