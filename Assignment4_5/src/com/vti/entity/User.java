package com.vti.entity;

public class User {
    int id;
    String name;
    String address;
    String phone;

    protected void test2() {
        System.out.println("Class cha User");
    }

    public User() {
        System.out.println("Đã vào đây");
    }

    public User(int id) {
        this.id = id;
        System.out.println("Hàm khởi tạo cha có tham số");
    }

    public User(float id) {
        System.out.println("Hàm khởi tạo cha có tham số 1");
    }

    public User(double id) {
        this(1);
        System.out.println("Hàm khởi tạo cha có tham số 2");
        // Muốn gọi tọi 1 hàm khởi tạo khác trong class
        // Thì sẽ sử dụng từ khóa this();
    }

}
