package com.vti.entity;

import com.vti.frontend.ScannerUtils;

public class KySu extends Canbo {

    private String nganhDaoTao;

    public KySu(String hoTen, int tuoi, Gioitinh gioitinh, String diaChi, String nganhDaoTao) {
        super(hoTen, tuoi, gioitinh, diaChi);
        this.nganhDaoTao = nganhDaoTao;
    }

    public String getNganhDaoTao() {
        return nganhDaoTao;
    }

    public void setNganhDaoTao() {
        this.nganhDaoTao = ScannerUtils.inputString();
    }

    @Override
    public String toString() {
        return "KySu{" +
                "nganhDaoTao='" + nganhDaoTao + '\'' +
                '}';
    }
}
