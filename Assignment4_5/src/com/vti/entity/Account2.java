package com.vti.entity;

public class Account2 {
    private String id;
    private String name;
    private int balance;

    public Account2(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return this.balance;
    }

    public int Credit(int amount) {
       return this.balance -= amount;
    }

    public int Debit(int amount) {
        return this.balance += amount;
    }

    public void tranferTo(Account2 account2, int amount){
        this.balance += amount;
        account2.balance -= amount;
    }
}
