package com.vti.entity;

import com.vti.frontend.ScannerUtils;

import java.util.PrimitiveIterator;

public class Canbo {
    private String hoTen;
    private int tuoi;
    private Gioitinh gioitinh;
    private String diaChi;


    public Canbo(String hoTen, int tuoi, Gioitinh gioitinh, String diaChi) {
        this.hoTen = hoTen;
        this.tuoi = tuoi;
        this.gioitinh = gioitinh;
        this.diaChi = diaChi;
    }

    public Canbo() {

    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen() {
        this.hoTen = ScannerUtils.inputString();
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi() {
        this.tuoi = ScannerUtils.inputNumber();
        while (this.tuoi <0){
            System.out.println("Mời bạn nhập lại tuổi");
            this.tuoi = ScannerUtils.inputNumber();
        }
    }

    public Gioitinh getGioitinh() {
        return gioitinh;
    }

    public void setGioitinh(Gioitinh gioitinh) {
        this.gioitinh = gioitinh;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi() {
        this.diaChi = ScannerUtils.inputString();
    }

    @Override
    public String toString() {
        return "Canbo{" +
                "hoTen='" + hoTen + '\'' +
                ", tuoi=" + tuoi +
                ", gioitinh=" + gioitinh +
                ", diaChi='" + diaChi + '\'' +
                '}';
    }
}
