package com.vti.entity;

import com.vti.frontend.ScannerUtils;

public class Student {
    private int id;
    private String name;
    private String hometown;
    private double point;

//    public Student(){
//        System.out.println("Nhập vào tên");
//        this.name = ScannerUtils.inputString();
//
//        System.out.println("Nhập vào địa chỉ");
//        this.hometown = ScannerUtils.inputString();
//    }


    public void setId() {
        this.id = ScannerUtils.inputNumberByminandmax(1, 50);
    }

    public void setName() {
        this.name = ScannerUtils.inputString();
    }

    public void setHometown() {
        this.hometown = ScannerUtils.inputString();
    }

    public void setPoint() {
        this.point = ScannerUtils.inputNumberByminandmax(0, 10);
    }

    public void inforStudent() {
        System.out.println("Nhập vào id học viên");
        setId();
        System.out.println("Nhập vào tên học viên");
        setName();
        System.out.println("Nhập vào nơi ở của học viên");
        setHometown();
        System.out.println("Nhập vào điểm hiện tại");
        setPoint();
    }

//    public Student() {
//        System.out.println("");
//        this.setId(0);
//        System.out.println("Nhập vào tên: ");
//        this.name = ScannerUtils.inputString();
//        System.out.println("Nhập vào nơi ở: ");
//        this.hometown = ScannerUtils.inputString();
//        this.point = 0;
//    }



    public double setPlusPoint() {
        System.out.println("Them bao nhiêu diem: ");
        float i = ScannerUtils.inputFoat();
        this.point = this.point + i;
        if(this.point > 10){
            System.out.println("Tổng điểm vượt quá quy định, mời thêm lại");
            i = ScannerUtils.inputFoat();
        } else {
            System.out.println("Điểm sau khi được cộng thêm" + this.point);
        }
        return this.point;
    }

    String rank = null;
    public String checkHocluc() {
        float i = (float) this.point;
        if (i < 4) {
            rank ="Học lực Yếu";
        } else if (4 < i & i < 6) {
            rank ="Học lực trung bình";
        } else if (6 < i & i < 8) {
            rank ="Học lực khá";
        } else {
            rank ="Học lực giỏi";
        }
        return rank;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + this.id +
                ", name='" + this.name + '\'' +
                ", hometown='" + this.hometown + '\'' +
                ", point=" + this.point +'\''+
                ", rank=" + rank +'\''+
                '}';
    }
}
