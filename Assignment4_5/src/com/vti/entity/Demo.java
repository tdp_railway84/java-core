package com.vti.entity;

import com.vti.frontend.ClassCon;
import com.vti.frontend.DemoAb;
import com.vti.frontend.DemoIn;
import com.vti.frontend.DemoInterface;

public class Demo {
    public static void main(String[] args) {
//        User user = new User();
//        user.test2();
//
//        Employee employee = new Employee();
//        employee.test2();

//        User user1 = new User(2d);
//        user1.test2();

        // Lỗi trong quá trình biên dịch (compile) có thể nhìn thấy trước khi chạy chương trình
        // lỗi trong quá trình runtime: KHi chạy chương trinh mới phát hiện ra lỗi
        DemoAb demoAb = new ClassCon();
        DemoInterface demoInterface = new DemoIn();
    }
}
