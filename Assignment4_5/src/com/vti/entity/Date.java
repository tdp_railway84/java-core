package com.vti.entity;

public class Date {
    private int Day;
    private int Month;
    private int year;

    public Date(int day, int month, int year) {
        this.Day = day;
        this.Month = month;
        this.year = year;
    }

    public int getDay() {
        return Day;
    }

    public void setDay(int day) {
        Day = day;
    }

    public int getMonth() {
        return Month;
    }

    public void setMonth(int month) {
        Month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    // kiểm tra có phải năm nhuận hay không
    public boolean isLeapYear() {
        boolean check = false;
        if (this.year % 4 == 0 & this.year % 100 != 0 || this.year % 400 == 0) {
            System.out.println("Đây là năm nhuận");
            check = true;
        } else
            System.out.println("Đây không phải năm nhuận");
        return check;
    }

    @Override
    public String toString() {
        return "Date{" +
                "Day=" + Day +
                ", Month=" + Month +
                ", year=" + year +
                '}';
    }
}
