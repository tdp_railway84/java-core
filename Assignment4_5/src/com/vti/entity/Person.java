package com.vti.entity;

import com.vti.frontend.ScannerUtils;

public abstract class Person {
    private String name;

    public void Person(String name){
        this.name = name;
    }

    public void setName() {
        this.name = ScannerUtils.inputString();
    }
}
