package com.vti.entity;

import com.vti.frontend.ScannerUtils;

public class Circle {
    private double radius;
    private String color;

    public Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius() {
        System.out.println("Mời bạn nhập vào bán kính");
        this.radius = ScannerUtils.inputDouble();
    }

    public String getColor() {
        return this.color;
    }

    public String setColor() {
        System.out.println("Mời bạn nhập vào màu sắc");
        this.color = ScannerUtils.inputString();
        return this.color;
    }
    private double area; // diện tích
    public double getArea() {
        this.area = Math.PI * Math.pow(radius,2);
        return area ;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                ", area=" + area +
                '}';
    }
}

