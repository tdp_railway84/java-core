package com.vti.entity;

import com.vti.frontend.ScannerUtils;

public class NhanVien extends Canbo {

    private String congViec;
    public NhanVien(String hoTen, int tuoi, Gioitinh gioitinh, String diaChi, String congViec) {
        super(hoTen, tuoi, gioitinh, diaChi);
        this.congViec = congViec;
    }

    public String getCongViec() {
        return congViec;
    }

    public void setCongViec() {
        this.congViec = ScannerUtils.inputString();
    }

    @Override
    public String toString() {
        return "NhanVien{" +
                "congViec='" + congViec + '\'' +
                '}';
    }
}
