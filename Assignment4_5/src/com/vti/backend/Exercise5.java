package com.vti.backend;

import com.vti.frontend.ScannerUtils;

public class Exercise5 {
    //tạo menu để chọn chức năng
    public void Question1_2(){
        menu();
    }

    public void menu() {

        do {
            System.out.println("\n"+ "-------------------------------------" + "\n"+
                    "Mời bạn chọn chức năng" + "\n"
                    + "1- Thêm mới cán bộ" + "\n"
                    + "2- Tìm kiếm theo họ tên" + "\n"
                    + "3- Hiển thị thông tin danh sách cán bộ" + "\n"
                    + "4- Delete cán bộ"+ "\n"
                    + "5- Thoát chương trình");
            int check = ScannerUtils.inputNumberByminandmax(1, 5);
            switch (check) {
                case 1:
//                    System.out.println("Thêm mới cán bộ");
                    ThemMoiCanbo();
                    break;
                case 2:
//                    System.out.println("Tìm kiếm theo họ tên");
                    Timkiem();
                    break;
                case 3:
//                    System.out.println("Hiển thị thông tin danh sách cán bộ");
                    HienThiDanhSach();
                    break;
                case 4:
//                    System.out.println("Delete cán bộ");
                    DeleteCanbo();
                    break;
                case 5:
                    System.out.println("Đang thoát chương trình...");
                    return;
            }
        } while (true);
    }

    private void ThemMoiCanbo() { // Thêm mới 1 đối tượng là cán bộ đó gán vào dsCB
        System.out.println("Mời bạn nhập cán bộ muốn thêm" + "\n"
                + "1- Thêm mới Công Nhân" + "\n"
                + "2- Thêm mới Kỹ Sư" + "\n"
                + "3- Thêm mới Nhân Viên");
        int check2 = ScannerUtils.inputNumberByminandmax(1, 4);
        switch (check2) {
            case 1:
                System.out.println("Thêm thông tin Công nhân");
                break;
            case 2:
                System.out.println("Thêm thông tin Kỹ Sư");
                break;
            case 3:
                System.out.println("Thêm thông tin Nhân Viên");
                break;
        }
        // Sau khi thêm thông tin sẽ add vào danh sách Cán bộ
        // Tiếp theo ấn nút để thoát chức năng này
    }

    private void Timkiem() { // Nhập vào tên dùng hàm for để tìm kiếm cán bộ đó trong danh sách Cán bộ
        System.out.println("Đang thực hiện tìm kiếm...");
    }

    private void HienThiDanhSach() { // Dùng hàm for để hiển thị thông tin danh sách cán bộ.
        System.out.println("Đang lọc danh sách...");
    }

    private void DeleteCanbo() { //Dùng hàm for để lọc danh sách cần xóa
        System.out.println("Cán bộ đã được xóa thông tin");
        System.out.println("Hiển thị danh sách cán bộ mới nhất");
    }


}
