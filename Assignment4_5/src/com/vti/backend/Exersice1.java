package com.vti.backend;

import com.vti.entity.Department;

public class Exersice1 {
    public void question1() {
        System.out.println("EX1 Question1");
        Department department1 = new Department();
        System.out.println(department1);

        Department department2 = new Department("DP2");
        System.out.println(department2);
    }

    public void question2() {
        System.out.println("EX1 Question1");
    }

    public void question3() {
        System.out.println("EX1 Question1");
    }
}
