package com.vti.frontend;

import java.util.Scanner;

public class ScannerUtils {
    static Scanner sc = new Scanner(System.in);

    //Tạo 1 method để nhập vào màn hình 1 số từ a cho tới b

    public static int inputNumber(){
        int number = sc.nextInt();
        return number;
    }

    public static float inputFloatNumber(float min, float max) {
        float number = sc.nextFloat();
        while (number < min || number > max) {
            System.out.println("Giá trị không thỏa mãn, mời bạn nhập lại");
            number = sc.nextFloat();
        }
        return number;
    }

    public static int inputNumberByminandmax(int min, int max) {
        int number = sc.nextInt();
        while (number < min || number > max) {
            System.out.println("Giá trị không thỏa mãn, mời bạn nhập lại");
            number = sc.nextInt();
        }
        return number;
    }


    // tạo method tạo tên
    public static String inputString() {
        return sc.nextLine();
    }

    // tạo method tạo số float
    public static float inputFoat() {
        return sc.nextFloat();
    }

    // tạo method tạo số double
    public static double inputDouble() {
        return sc.nextDouble();
    }

    // tạo 1 method để nhập vào 1 chuỗi có định dạng là email(có chứa @)
    public static String inputEmail() {
        String email = sc.nextLine();
        while (!email.contains("@")) {
            System.out.println("Email cần có kí tự @, mời bạn nhập lại");
            email = sc.nextLine();
        }
        return email;
    }

    // tạo ra 1 method nhập vào định dạng là 1 số điện thoại: bắt đầu là số 0
    public static String inputPhoneNumber() {
        String phoneNumber = sc.nextLine();
        char check1 = phoneNumber.charAt(0);
        int check2 = phoneNumber.length();
        while (check1 != 0) {
            System.out.println("Số điện thoại phải bắt đầu là số 0, mời bạn nhập lại");
            phoneNumber = sc.nextLine();
            while (check2 < 0 || check2 > 13) {
                System.out.println("Số điện thoại bao gồm 11 kí tự, mời bạn nhập lại");
                phoneNumber = sc.nextLine();
            }
        }
        return phoneNumber;
    }

    // tạo ra 1 method nhập vào mật khẩu tùy ý có 9 đến 12 ký tự

    // tạo ra 1 method nhập vào giới tính

}
