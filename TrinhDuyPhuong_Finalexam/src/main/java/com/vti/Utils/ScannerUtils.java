package com.vti.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class ScannerUtils {
    static Scanner sc = new Scanner(System.in);

    //Nhập số
    public static int inputNumber(String errorMessenger) {
        while (true) {
            try {
                String number = sc.nextLine();
                return Integer.parseInt(number);
            } catch (Exception e) {
                System.out.println(errorMessenger);
            }
        }
    }

    //Nhập số điện thoại
    public static int inputPhoneNumber() {
        while (true) {
            try {
                String number = sc.nextLine();
                if (number == null || number.length() > 12) {
                    System.out.println("Số điện thoại bao gồm 12 sô, mời nhập lại");
                    number = sc.nextLine();
                } else if (number.charAt(0) != 0) {
                    System.out.println("Số điện thoại phải bắt đầu bằng số 0, mời nhập lại");
                } else {
                    return Integer.parseInt(number);
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    //Nhập số nằm trong khoảng min max
    public static int inputNumberByMinMax(int min, int max) {
        int number = sc.nextInt();
        try {
            while (number < min || number > max) {
                System.out.println("Giá trị phải là số trong khoảng từ: " + min + "đến " + max + ", mời nhập lại");
                number = sc.nextInt();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return number;
    }

    //Nhập chuỗi
    public static String inputString(String errorMessenger) {
        while (true) {
            String input = sc.nextLine();
            if (input.isEmpty()) {
                System.out.println(errorMessenger);
                continue;
            } else {
                return input;
            }
        }
    }

    //Nhập email có kí tự @
    public static String inputEmail() {
        while (true) {
            String email = sc.nextLine();
            //không để trống
            if (email.isEmpty()) {
                System.out.println("Email không được để trống, mời nhập lại");
                continue;
            } else if (!email.contains("@gmail.com")) {
                System.out.println("Email phải có ký tự @gmail.com, mời nhập lại");
                continue;
            } else {
                return email;
            }
        }
    }

    //Nhập ngày tháng
    public static Date inputDate(String errorMessenger) {
        while (true) {
            String stringDate = sc.nextLine();
            try {
                if (stringDate.isEmpty()) {
                    System.out.println("Mời nhập vào ngày tháng");
                    continue;
                }
                DateFormat format = new SimpleDateFormat("YYYY-MM-DD", Locale.ENGLISH);
                Date date = format.parse(stringDate);
                return date;
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.out.println(errorMessenger);
            }
        }
    }

    //Nhập mật khẩu yêu cầu có ít nhất 1  kí tự viết hoa
    public static String inputPassword() {
        while (true) {
            String password = sc.nextLine();
            try {
                if (password.isEmpty()) {
                    System.out.println("Mật khẩu không được để trống, mời nhập lại.");
                    continue;
                }
                if (password.length()<6 || password.length()>12){
                    System.out.println("Mật khẩu chỉ từ 6 đến 12 ký tự, mời nhập lại");
                    continue;
                }
                boolean hasUpperCase = false;
                for (int i = 0; i < password.length(); i++) {
                    char ch = password.charAt(i);
                    if (Character.isUpperCase(ch)) {
                        hasUpperCase = true;
                        break;
                    }
                }
                if (!hasUpperCase) {
                    System.out.println("Mật khẩu phải có ít nhất 1 kí tự viết hoa, mời nhập lại.");
                    continue;
                }
                return password;
            } catch (Exception e)
            {
                System.out.println("Lỗi: " + e.getMessage());
            }
        }
    }
}
