package com.vti.backend.service;


import com.vti.entity.Employee;
import com.vti.entity.Manager;
import com.vti.entity.User;

import java.util.List;

public interface UserService {
    User login(String email, String password);

    List<Employee> getEmployee(String projectId);

    List<Manager> getAllManager();
}
