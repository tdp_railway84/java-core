package com.vti.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Manager extends User{
    private int expInYear;
    private String projectId;

    public Manager() {
        super();
    }

    public Manager(int id, String fullName, String email, String password, Role role, int expInYear, String projectId) {
        super(id, fullName, email, password, role);
        this.expInYear = expInYear;
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "expInYear=" + expInYear +
                ", projectId='" + projectId + '\'' +
                "} " + super.toString();
    }
}
