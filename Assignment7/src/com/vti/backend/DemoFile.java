package com.vti.backend;

import java.io.File;
import java.io.IOException;

public class DemoFile {
    public static boolean checkFileExist(String pathFile){
        File file = new File(pathFile);
        boolean check;
        if(file.exists()){
            System.out.println("File tồn tại");
            return true;
        }else {
            System.out.println("File không tồn tại");
            return false;
        }
    }

    public static void createFile(String fileName) {
        String Data = "Assignment7/Data/" + fileName;
        File file = new File(Data);
        try {
            if (file.createNewFile()) {
                System.out.println("Đã tạo được file");
            } else {
                System.out.println("Chưa tạo được file");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void deleteFile(String fileName){
        File file = new File(fileName);
        if (file.exists()){
            System.out.println(file.delete() ? "File tồn tại " : "File không tồn tại");
        }
    }
}
