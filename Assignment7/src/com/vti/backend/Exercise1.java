package com.vti.backend;

import com.vti.entity.StudentEx1;

import java.util.ArrayList;

public class Exercise1 {
    public void Question1() {
        ArrayList<StudentEx1> studentArrayList = new ArrayList<>();
        StudentEx1 student1 = new StudentEx1("Trinh Duy Phuong");
        StudentEx1 student2 = new StudentEx1("Dinh Khac Son");
        StudentEx1 student3 = new StudentEx1("Nguyen Tien Anh");
        studentArrayList.add(student1);
        studentArrayList.add(student2);
        studentArrayList.add(student3);

        for (StudentEx1 studentList : studentArrayList) {
            System.out.println(studentList);
        }

        StudentEx1.college = "Đại học Công nghệ";
        for (StudentEx1 studentList : studentArrayList) {
            System.out.println(studentList);
        }
    }
}
