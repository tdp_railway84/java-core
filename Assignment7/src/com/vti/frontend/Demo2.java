package com.vti.frontend;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Demo2 {
    public static void main(String[] args) {
//        List<Integer> dsSo = new ArrayList<>(); In ra được các giá trị trùng nhau
//        dsSo.add(1);
//        dsSo.add(2);
//        dsSo.add(1);
//        System.out.println(dsSo);
        Set<Integer> dsSet = new HashSet<>(); //không in được các giá trị trùng nhau
        dsSet.add(1);
        dsSet.add(2);
        dsSet.add(1);
        System.out.println(dsSet);

        Map<String,Integer> map = new HashMap<>();
        map.put("key1", 10);
        map.put("key2", 5);
        System.out.println(map.get("key2"));
    }
}
