package com.vti.entity;

import com.vti.frontend.ScannerUtils;

public class Student2Ex2 {
    private final  int id = 1;
    private String name;

    public Student2Ex2() {
        this.name = ScannerUtils.inputString();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public final  void study(){
        System.out.println("Đang học bài....");
    }
}
