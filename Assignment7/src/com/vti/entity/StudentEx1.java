package com.vti.entity;

public class StudentEx1 {
    private int id;
    private String name;
    private static int number = 1;
    public static String college = "Đại học Bách Khoa";

    public StudentEx1(String name){
        this.name = name;
        this.id = StudentEx1.number;
        StudentEx1.number++;
    }

//    public static String getSchool(){
//        return college;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", school= " + college +'\'' +
                '}';
    }
}
