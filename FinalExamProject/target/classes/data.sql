create table if not exists User(
                                   id int auto_increment primary key ,
                                   full_name nvarchar(50) not null ,
                                   email varchar(50) not null unique ,
                                   password varchar(30) not null ,
                                   exp_in_year int,
                                   pro_skill varchar(30),
                                   project_id varchar(30),
                                   role enum('manager','employee')
);

INSERT INTO finalexamdb.user (full_name, email, password, exp_in_year, pro_skill, project_id, role)
VALUES  ('trinh duy phuong', 'phuongtd@gmail.com', 'Phuong123', 2, null, 'A12', 'manager'),
        ('nguyen van B', 'Bnv@gmail.com', 'H1234bnv', null, 'dev', 'A123', 'employee'),
        ('nguyen van C', 'Cnv@gmail.com', 'H1234cnv', null, 'java', 'A123', 'employee'),
        ('nguyen van D', 'Dnv@gmail.com', 'H1234dnv', null, 'sql', 'A1234', 'employee'),
        ('nguyen van E', 'Env@gmail.com', 'H1234env', 3, null, 'A1234', 'manager');
