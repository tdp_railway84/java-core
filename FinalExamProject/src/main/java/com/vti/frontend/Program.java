package com.vti.frontend;

import com.vti.Utils.ScannerUtils;

public class Program {
    Function function = new Function();

    public static void main(String[] args) {
        Program program = new Program();
        program.menu();
    }

    private void menu() {
        while (true) {
            System.out.println("Menu chương trình, mời bạn chọn");
            System.out.println("Tính năng bạn chọn là gì ?" + "\n" +
                    "1- Tìm employee theo project id" + "\n" +
                    "2- Lấy tất cả manager" + "\n" +
                    "3- Đăng nhập hệ thống bằng Manager" + "\n" +
                    "4- Thoát chương trình ");
            int choose = ScannerUtils.inputNumberByMinMax(1,4);
            switch (choose){
                case 1:
                    function.findEmployByProjectID();
                    break;
                case 2:
                    function.getAllManager();
                    break;
                case 3:
                    function.login();
                case 4:
                    System.out.println("Đã thoát chương trình");
                    return;
            }
        }
    }

}

