package com.vti.frontend;

import com.vti.Utils.ScannerUtils;
import com.vti.backend.controller.UserController;
import com.vti.entity.Employee;
import com.vti.entity.Manager;
import com.vti.entity.Role;
import com.vti.entity.User;

import java.util.List;

public class Function {
    private UserController userController = new UserController();

    public void login() {
        while (true) {
            System.out.println("Mời bạn nhập vào email");
            String email = ScannerUtils.inputEmail();

            System.out.println("Mời bạn nhập vào password");
            String password = ScannerUtils.inputPassword();

            User user = userController.login(email, password);
            if (user.getRole() == Role.manager){
                System.out.println(user);
            }
            else if (user == null) {
                System.out.println("Email hoặc password sai, mời bạn nhập lại");
                continue;
            } else {
                System.out.println("Thông tin nhập vào không phải của manager, mời nhập lại");
            }
        }
    }

    public void findEmployByProjectID() {
        System.out.println("Lấy ra Employee");
        List<Employee> employList = null;
        try {
            System.out.println("Mời bạn nhập vào project Id");
            String projectId = ScannerUtils.inputString("Không để trống");
            employList = userController.getEmployee(projectId);
            System.out.println("|--------------------------------------------------------------------------------------------------|");
            System.out.println("|   Id   |      FullName      |        Email        |     ProSkill    |     ProtectId  |     Role  |");
            System.out.println("|--------------------------------------------------------------------------------------------------|");
            for (Employee employees : employList
            ) {
                System.out.printf("|%4s    |     %-12s   |      %-13s  |     %-10s  |     %-8s   |  %-7s |%n", employees.getId(), employees.getFullName(), employees.getEmail(), employees.getProSkill(), employees.getProjectId(), employees.getRole());
            }
        } catch (Exception e) {
            System.err.println("Lỗi: " + e.getMessage());
        }
    }

    public void getAllManager(){
        System.out.println("Lấy ra tất cả Manager");
        List<Manager> managerList = null;
        try {
            managerList = userController.getAllManager();
            System.out.println("|---------------------------------------------------------------------------------------------------|");
            System.out.println("|   Id   |      FullName      |        Email        |     ExpInYear    |     ProtectId  |     Role  |");
            System.out.println("|---------------------------------------------------------------------------------------------------|");
            for (Manager managers : managerList){
                System.out.printf("|%4s    |     %-12s   |      %-13s  |     %-10s  |     %-8s   |  %-7s |%n", managers.getId(), managers.getFullName(), managers.getEmail(), managers.getExpInYear(), managers.getProjectId(), managers.getRole());
            }
            System.out.println();
        } catch (Exception e){
            System.err.println("Lỗi: " + e.getMessage());
        }
    }
}

