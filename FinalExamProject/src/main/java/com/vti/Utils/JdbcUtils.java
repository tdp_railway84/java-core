package com.vti.Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class JdbcUtils {
    static Connection connection = null;

    public static Connection GetConnectionDB() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("FinalExamProject/src/main/resources/db.properties"));
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        String userName = properties.getProperty("user");
        String password = properties.getProperty("password");
        String url = properties.getProperty("url");
        String driver = properties.getProperty("driver");

        try {
            Class.forName(driver); //checked
            connection = DriverManager.getConnection(url,userName,password);
            if (connection!= null){
                System.out.println("Connection Successful");
            }else {
                System.out.println("Connection Unsuccessful, Please check once more");
            }
        } catch (Exception ex){
            System.err.println(ex.getMessage());
            return null;
        }
        return connection;
    }

    public static Connection GetDisConnectionDB(){
            try {
                if (connection!= null){
                    connection.close();
                    System.out.println("Disconnected successfully");
                }
            }catch (Exception ex){
                System.err.println(ex.getMessage());
            }
        return connection;
    }

/*    //check connection
    public static void main(String[] args) {
        GetConnectionDB();
        GetDisConnectionDB();
    }*/
}
