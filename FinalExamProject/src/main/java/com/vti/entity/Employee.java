package com.vti.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Employee extends User {
    private String projectId;
    private String proSkill;

    public Employee(int id, String fullName, String email, String password, Role role, String projectId, String proSkill) {
        super(id, fullName, email, password, role);
        this.projectId = projectId;
        this.proSkill = proSkill;
    }

    public Employee() {
        super();
    }

    @Override
    public String toString() {
        return "Employee{" +
                "projectId='" + projectId + '\'' +
                ", proSkill='" + proSkill + '\'' +
                "} " + super.toString();
    }
}
