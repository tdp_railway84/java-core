package com.vti.backend.repository;

import com.vti.Utils.JdbcUtils;
import com.vti.entity.Employee;
import com.vti.entity.Manager;
import com.vti.entity.Role;
import com.vti.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    public User login(String email, String password) {
        User user = null;
        try {
            String sql = "select u.id,u.full_name,u.email,u.password,u.role from finalexamdb.user as u where email = ? AND password = ?";
            Connection connection = JdbcUtils.GetConnectionDB();

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                user = new User();
                user.setId(resultSet.getInt("u.id"));
                user.setFullName(resultSet.getString("u.full_name"));
                user.setEmail(resultSet.getString("u.email"));
                user.setPassword(resultSet.getString("u.password"));
                user.setRole(Role.valueOf(resultSet.getString("u.role")));
            } else {
                System.out.println("user name và password không tồn tại manager nào, mời nhập lại");
            }
        } catch (Exception e) {
            System.err.println("Lỗi: " + e.getMessage());
        } finally {
            JdbcUtils.GetDisConnectionDB();
        }
        return user;
    }

    //Nhập id project sau đó in ra các employee của project
    public List<Employee> getEmployeeByProjectId(String projectId) {
        List<Employee> employeeList = new ArrayList<>();
        try {
            String sql = "Select u.id, u.full_name, u.email, u.pro_skill, u.project_id, u.role from finalexamdb.user u where project_id = ? AND u.role = 'employee'";
            Connection connection = JdbcUtils.GetConnectionDB();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, projectId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Employee employee = new Employee();

                employee.setId(resultSet.getInt("u.id"));
                employee.setFullName(resultSet.getString("u.full_name"));
                employee.setEmail(resultSet.getString("u.email"));
                employee.setProSkill(resultSet.getString("u.pro_skill"));
                employee.setProjectId(resultSet.getString("u.project_id"));
                employee.setRole(Role.valueOf(resultSet.getString("u.role")));

                employeeList.add(employee);
            }
        } catch (Exception e) {
            System.err.println("Lỗi: " + e.getMessage());
        } finally {
            JdbcUtils.GetDisConnectionDB();
        }
        return employeeList;
    }

    //    In ra các manager của project
    public List<Manager> getAllManager() {
        List<Manager> managerList = new ArrayList<>();
        try {
            String sql = "select u.id, u.full_name, u.email, u.exp_in_year, u.project_id, u.role from finalexamdb.User As u where role like '%manager'";
            Connection connection = JdbcUtils.GetConnectionDB();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                Manager manager = new Manager();
                String roleString = resultSet.getString("role");
                Role roleEnum = Role.valueOf(roleString);

                manager.setId(resultSet.getInt("u.id"));
                manager.setFullName(resultSet.getString("u.full_name"));
                manager.setEmail(resultSet.getString("u.email"));
                manager.setExpInYear(resultSet.getInt("u.exp_in_year"));
                manager.setProjectId(resultSet.getString("u.project_id"));
                manager.setRole(roleEnum);

                managerList.add(manager);
            }
        } catch (Exception e) {
            System.err.println("Lỗi: " + e.getMessage());
        } finally {
            JdbcUtils.GetDisConnectionDB();
        }
        return managerList;
    }
}
