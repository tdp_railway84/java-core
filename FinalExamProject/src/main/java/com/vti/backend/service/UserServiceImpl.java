package com.vti.backend.service;

import com.vti.backend.repository.UserRepository;
import com.vti.entity.Employee;
import com.vti.entity.Manager;
import com.vti.entity.User;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserRepository userRepository = new UserRepository();

    @Override
    public User login(String email, String password) {
        return userRepository.login(email,password);
    }

    @Override
    public List<Employee> getEmployee(String projectId) {
        return userRepository.getEmployeeByProjectId(projectId);
    }

    @Override
    public List<Manager> getAllManager() {
        return userRepository.getAllManager();
    }


}
