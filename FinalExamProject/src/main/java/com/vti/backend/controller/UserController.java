package com.vti.backend.controller;

import com.vti.backend.service.UserService;
import com.vti.backend.service.UserServiceImpl;
import com.vti.entity.Employee;
import com.vti.entity.Manager;
import com.vti.entity.User;

import java.util.List;

public class UserController {

    private UserService userService = new UserServiceImpl();

    public User login(String email, String password){
        return userService.login(email,password);
    }

    public List<Employee> getEmployee(String projectId){
        return userService.getEmployee(projectId);
    }

    public List<Manager> getAllManager(){
        return userService.getAllManager();
    }
}
