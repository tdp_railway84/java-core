public class Exercise1 {
    public void Question1(Account acc) {
        if (acc == null) {
            System.out.println("Nhân viên này chưa có phòng ban nào");
        } else
            System.out.println("Phòng ban của nhân viên này là: " + acc.departmentId.departmentName);
    }

    public void Question2(Account acc) {
        int size = acc.groups.length;
        if (size == 0) {
            System.out.println("Nhân viên này chưa có group");
        } else if (0 < size && size <= 2) {
            System.out.println("Group của nhân viên này là: " + acc.groups + "\n");
        } else
            System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
    }

    public void Question3(Account acc) {
        System.out.println(acc.departmentId.departmentId == 0 ?
                "Nhân viên này chưa có phòng ban nào" : "Phòng ban của nhân viên là: " + acc.departmentId.departmentName);
    }

    public void Question4(Account acc) {
        System.out.println("Dev".equals(acc.positionId.positionName.name()) ? "Đây là Developer"
                : "Người ngày không phải Deverloper");
    }


    public void Question5(Group[] group) {
        int count = group.length;
        switch (count) {
            case 1:
                System.out.println("Nhóm có một nhân viên");
                break;
            case 2:
                System.out.println("Nhóm có hai nhân viên");
                break;
            case 3:
                System.out.println("Nhóm có ba nhân viên");
                break;
            default:
                System.out.println("Nhóm chưa có thành viên nào");
                return;
        }
    }

    public void Question6(Account acc) {
        int size = acc.groups.length;
        switch (size) {
            case 1:
                System.out.println("Nhân viên này ở trong 1 group là: " + acc.groups.toString());
                break;
            case 2:
                System.out.println("Nhân viên này ở trong 2 group là: " + acc.groups.toString());
                break;
            case 3:
                System.out.println("Nhân viên này là nhân vật quan trọng");
                break;
            default:
                System.out.println("Nhân viên này chưa thuộc nhóm nào");
                return;
        }
    }

    public void Question7(Account acc) {
        int id = acc.positionId.positionId;
        switch (id) {
            case 1:
                System.out.println("Đây là developer");
                break;
            default:
                System.out.println("Người này không phải là Devoloper");
                return;
        }
    }


    public void Question8(Account[] accounts) {
        for (Account acc : accounts) {
            System.out.println("Thông tin Account " + acc.accountId + ": \n" + "email: " + acc.email + "\n"
                    + "fullName: " + acc.fullName + "\n" + "Thuộc phòng: " + acc.departmentId.departmentName);
        }
    }

    public void Question9(Department[] departments) {
        for (Department dep : departments) {
            System.out.println("id: " + dep.departmentId + "\n" + "name: " + dep.departmentName);
        }
    }

    public void Question10(Account[] accounts) {
        for (int i = 0; i < accounts.length; i++) {
            System.out.println("Thông tin Account " + (i + 1) + ": \n" + "email: " + accounts[i].email + "\n"
                    + "fullName: " + accounts[i].fullName + "\n" + "Thuộc phòng: " + accounts[i].departmentId.departmentName);
        }
    }

    public void Question11(Department[] deps) {
        for (int i = 0; i < deps.length; i++) {
            System.out.println("Thông tin department thứ " + (i + 1) + "là: \n" + "id: "
                    + deps[i].departmentId + "\n" + "Name: " + deps[i].departmentName + "\n");
        }
    }

    public void Question12(Account[] accounts) {
        for (int i = 0; i < accounts.length - 1; i++) {
            System.out.println("Thông tin Account " + (i + 1) + ": \n" + "Department: " + accounts[i].departmentId.departmentName);
        }
    }

    public void Question13(Account[] accounts) {
        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].accountId == 2) {
                continue;
            } else System.out.println("Thông tin Account " + (i + 1) + ": \n" + "email: " + accounts[i].email + "\n"
                    + "fullName: " + accounts[i].fullName + "\n" + "Thuộc phòng: " + accounts[i].departmentId.departmentName);
        }
    }

    public void Question14(Account[] accounts) {
        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].accountId < 4) {
                System.out.println("Thông tin Account " + (i + 1) + ": \n" + "email: " + accounts[i].email + "\n"
                        + "fullName: " + accounts[i].fullName + "\n" + "Thuộc phòng: " + accounts[i].departmentId.departmentName);
            }
        }
    }

    public void Question15() {
        System.out.println("Dãy số sau khi chọn lọc là: ");
        for (int i = 0; i <= 20; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }

    public void Question17_10(Account[] accounts) {
        // Question 10
        int size = accounts.length;
        do {
            Account account = accounts[size - 1];
            System.out.println(account.accountId);
            size--;
        }
        while (size >= 1);
    }

    public void Question17_12(Account[] accounts) {
        // In ra thông tin 2 account đầu tiên
        int size = accounts.length;
        int i = 0;
        do {
            Account account = accounts[i];
            System.out.println(account.accountId);
            i++;
            if (i == 2) {
                break;
            }
        } while (i < size);
    }

    public void Question17_15() {
        int i = 0;
        do {
            if (i % 2 != 0) {
                System.out.println(i);
                i=i+2;
            }
        }
        while (i<=20);
    }
}


