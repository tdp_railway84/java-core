import java.time.LocalDate;


public class Exam {
    int examId;
    String code;
    String title;
    CategoryQuestion categoryId;
    int duration;
    Account creatorId;
    LocalDate createDate;
    Question[] questions;
}
