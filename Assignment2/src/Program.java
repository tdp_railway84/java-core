import java.text.ParseException;
import java.time.LocalDate;

public class Program {
    public static void main(String[] args) throws ParseException {
// Thêm dữ liệu phòng ban:
        Department dep1 = new Department();
        dep1.departmentId = 1;
        dep1.departmentName = "Sale";

        Department dep2 = new Department();
        dep2.departmentId = 2;
        dep2.departmentName = "Marketing";

        Department dep3 = new Department();
        dep3.departmentId = 3;
        dep3.departmentName = "Engineer";

// Thêm dữ liệu position:
        Position pos1 = new Position();
        pos1.positionId = 1;
        pos1.positionName = PositionName.Dev;

        Position pos2 = new Position();
        pos2.positionId = 2;
        pos2.positionName = PositionName.Test;

        Position pos3 = new Position();
        pos3.positionId = 3;
        pos3.positionName = PositionName.Scrum_Master;

        Position pos4 = new Position();
        pos4.positionId = 4;
        pos4.positionName = PositionName.PM;

        // Thêm dữ liệu account:

        Account acc1 = new Account(1,"duyphuong1@gmail.com","duyphuong1", "duy phuong");
//        acc1.accountId = 1;
//        acc1.email = "duyphuong1@gmail.com";
//        acc1.fullName = "duy phuong1";
//        acc1.userName = "duyphuong1";
//        acc1.departmentId = dep1;
//        acc1.positionId = pos1;
//        acc1.createDate = LocalDate.now();

        Account acc2 = new Account(2, "duyphuong2@gmail.com","duyphuong2", "duy phuong");
//        acc2.accountId = 2;
//        acc2.email = "duyphuong2@gmail.com";
        acc2.fullName = "duy phuong2";
        acc2.userName = "duyphuong2";
        acc2.departmentId = dep2;
        acc2.positionId = pos2;
        acc2.createDate = LocalDate.of(2024, 1, 18);

        Account acc3 = new Account(3,"duyphuong3@gmail.com","duyphuong2", "duy phuong");
//        acc3.accountId = 3;
//        acc3.email = "duyphuong3@gmail.com";
        acc3.fullName = "duy phuong3";
        acc3.userName = "duyphuong3";
        acc3.departmentId = dep3;
        acc3.positionId = pos3;
        acc3.createDate = LocalDate.of(2023, 12, 12);

        Account acc4 = new Account(4,"duyphuong4@gmail.com","duyphuong4", "duy phuong");
//        acc4.accountId = 4;
//        acc4.email = "duyphuong4@gmail.com";
        acc4.fullName = "duy phuong4";
        acc4.userName = "duyphuong4";
        acc4.departmentId = dep3;
        acc4.positionId = pos4;
        acc4.createDate = LocalDate.of(2023, 6, 25);

        System.out.println(acc1);
        // Thêm dữ liệu Group:
        Group gr1 = new Group();
        gr1.groupId = 1;
        gr1.groupName = "Group1";
        gr1.creatorId = acc1;
        gr1.createDate = LocalDate.now();
        Account[] accounts1 = {acc1, acc2};
        gr1.accounts = accounts1;

        Group gr2 = new Group();
        gr2.groupId = 2;
        gr2.groupName = "Group2";
        gr2.creatorId = acc2;
        gr2.createDate = LocalDate.now();
        Account[] accounts2 = {acc1, acc3};
        gr2.accounts = accounts2;

        Group gr3 = new Group();
        gr3.groupId = 2;
        gr3.groupName = "Group2";
        gr3.creatorId = acc2;
        gr3.createDate = LocalDate.now();
        Account[] accounts3 = {acc1, acc2, acc3};
        gr3.accounts = accounts3;

        // gán Group cho Account:
        Group[] groups1 = {gr1, gr2};
        acc1.groups = groups1;

        Group[] groups2 = {gr1, gr2, gr3};
        acc2.groups = groups2;

        Group[] groups3 = {gr2, gr3};
        acc3.groups = groups3;

        // Thêm dữ liệu TypeQuestion:

        TypeQuestion type1 = new TypeQuestion();
        type1.typeId = 1;
        type1.typeName = TypeName.Essay;

        TypeQuestion type2 = new TypeQuestion();
        type2.typeId = 2;
        type2.typeName = TypeName.Multiple_Choise;

        // Thêm dữ liệu CataloryQuestion:

        CategoryQuestion category1 = new CategoryQuestion();
        category1.categoryId = 1;
        category1.categoryName = CategoryQue_Name.Java;

        CategoryQuestion category2 = new CategoryQuestion();
        category2.categoryId = 2;
        category2.categoryName = CategoryQue_Name.Net;

        CategoryQuestion category3 = new CategoryQuestion();
        category2.categoryId = 3;
        category2.categoryName = CategoryQue_Name.Ruby;

        // Thêm dữ liệu Question:

        Question que1 = new Question();
        que1.questionId = 1;
        que1.content = "content1";
        que1.categoryId = category1;
        que1.creatorId = acc1;
        que1.typeId = type1;
        que1.createDate = LocalDate.now();

        Question que2 = new Question();
        que2.questionId = 2;
        que2.content = "content2";
        que2.categoryId = category2;
        que2.creatorId = acc2;
        que2.typeId = type2;
        que2.createDate = LocalDate.now();

        Question que3 = new Question();
        que3.questionId = 3;
        que3.content = "content3";
        que3.categoryId = category3;
        que3.creatorId = acc3;
        que3.typeId = type2;
        que3.createDate = LocalDate.now();

        // Thêm dữ liệu Answer:
        Answer ans1 = new Answer();
        ans1.answerId = 1;
        ans1.content = "content1";
        ans1.questionId = que1;
        ans1.isCorrect = true;

        Answer ans2 = new Answer();
        ans2.answerId = 2;
        ans2.content = "content2";
        ans2.questionId = que2;
        ans2.isCorrect = false;

        // Thêm dữ liệu Exam:
        Exam exam1 = new Exam();
        exam1.examId = 1;
        exam1.code = "Code1";
        exam1.categoryId = category1;
        exam1.duration = 60;
        exam1.title = "Title1";
        exam1.creatorId = acc1;
        exam1.createDate = LocalDate.now();

        Exam exam2 = new Exam();
        exam2.examId = 2;
        exam2.code = "Code2";
        exam2.categoryId = category2;
        exam2.duration = 60;
        exam2.title = "Title2";
        exam2.creatorId = acc2;
        exam2.createDate = LocalDate.now();

        Exam exam3 = new Exam();
        exam3.examId = 3;
        exam3.code = "Code3";
        exam3.categoryId = category2;
        exam3.duration = 60;
        exam3.title = "Title3";
        exam3.creatorId = acc3;
        exam3.createDate = LocalDate.now();

/*        Exercise1 exe1_1 = new Exercise1();
        exe1_1.Question1(acc2);
        exe1_1.Question2(acc2);
        exe1_1.Question3(acc2);
        exe1_1.Question4(acc1);

        Exercise1 exe2_1 = new Exercise1();
        exe2_1.Question5(groups1);
        exe2_1.Question6(acc1);
        exe2_1.Question7(acc1);

        Exercise1 exe3_1 = new Exercise1();
        exe3_1.Question8(accounts1);

        Department[] deps = {dep1, dep2, dep3};
        exe3_1.Question9(deps);

        exe3_1.Question10(accounts1);
        exe3_1.Question11(deps);
        exe3_1.Question12(accounts1);
        exe3_1.Question13(accounts2);
        exe3_1.Question14(accounts1);
        exe3_1.Question15();
        exe3_1.Question17_10(accounts1);
        exe3_1.Question17_12(accounts1);
        exe3_1.Question17_15();
        System.out.println("hello world");*/

/*        Exercise4 exe4 = new Exercise4();
//        exe4.Question1();
//        exe4.Question2();
//        exe4.Question3();
//        exe4.Question4();
//        exe4.Question5();
//        exe4.Question6();
//        exe4.Question7();

        Exercise5 exe5_1 = new Exercise5();
//        exe5_1.Question1();
//        exe5_1.Question2();
//        exe5_1.Question3();
//        exe5_1.Question4();
//        exe5_1.Question5();
//        exe5_1.Question6();
//        exe5_1.Question7();
//        Exercise6 exe6 = new Exercise6();
//        exe6.Question1();*/

    }
}
