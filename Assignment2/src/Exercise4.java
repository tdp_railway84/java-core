
import java.time.LocalDate;
import java.util.Random;

public class Exercise4 {
    Random random = new Random();

    public void Question1() {
        int intNumber = random.nextInt();
        System.out.println("Hệ thống sẽ in ra 1 số nguyên bất kỳ: " + intNumber);
    }

    public void Question2() {
        float floatNumber = random.nextFloat();
        System.out.println("Hệ thống sẽ in ra 1 số thực bất kỳ: " + floatNumber);
    }

    public void Question3() {
        int cout = 0;
        do {
            String[] students = {"Hoàng", "Tuấn", "Phong", "Hiếu" };
            int randomNumber = random.nextInt(students.length);
            System.out.println("Tên học sinh được lấy ngẫu nhiên: " + students[randomNumber]);
        cout++;
        } while (cout < 5);
    }

    public void Question4(){
        Long minday = LocalDate.of(1995,7,24).toEpochDay();
        Long maxday = LocalDate.of(1995,12,20).toEpochDay();
        Long randomDay = minday + random.nextInt((int) (maxday-minday));

        // Chuyển epoch day thành LocalDate
        LocalDate date = LocalDate.ofEpochDay(randomDay);
        System.out.println("Ngày ngẫu nhiên: " + date);
    }
    public void Question5(){
        Long maxday = LocalDate.now().toEpochDay();
        Long minday = LocalDate.now().toEpochDay()-365;
        Long randomDay = minday + random.nextInt((int) (maxday-minday));

        LocalDate date = LocalDate.ofEpochDay(randomDay);
        System.out.println("Ngày ngẫu nhiên trong 1 năm trở lại đây: " + date);
    }
    public void Question6(){
        Long starDay = LocalDate.MIN.toEpochDay();
        Long endDay = LocalDate.now().toEpochDay();
        int randomDay =  random.nextInt((int) (endDay-starDay));

        LocalDate date = LocalDate.ofEpochDay(randomDay);
        System.out.println("Ngày bất kỳ trong quá khứ: " + date);
    }
    public void Question7(){
        int randomInt = random.nextInt(999-100) + 100;
        System.out.println(randomInt);
    }
}

