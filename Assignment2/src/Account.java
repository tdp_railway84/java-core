import java.time.LocalDate;

public class Account {
    int accountId;
    String email;
    String userName;
    String fullName;
    Department departmentId;
    Position positionId;
    LocalDate createDate;
    Group[] groups;

    public Account(int id, String email, String userName, String fullName){
        this.accountId = id;
        this.email = email;
        this.userName = userName;
        this.fullName = fullName;
    }


    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                ", email='" + email + '\'' +
                '}';
    }
    //hàm khởi tạo không tham số
}
