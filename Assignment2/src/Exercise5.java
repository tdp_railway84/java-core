import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Exercise5 {
    public void Question1(){
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("Nhập vào số nguyên bất kỳ từ bàn phím: ");
            int a = sc.nextInt();
            int b = sc.nextInt();
            int c = sc.nextInt();
            System.out.println("Số thứ nhất: " + a);
            System.out.println("Số thứ hai: " + b);
            System.out.println("Số thứ ba: " + c);
//           sc.close();
        } while (true);
    }

    public void Question2() {
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("Mời bạn nhập 1 số thực từ bàn phím: ");
            float d = sc.nextFloat();
            System.out.println("Số bạn vừa nhập là: " + d);
        } while (true);
    }

    public void Question3() {
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Mới bạn nhập vào họ và tên: ");
            String name = sc.nextLine();
            System.out.println("Tên của bạn là: " + name);
        } while (true);
    }

    public void Question4() throws ParseException {
        Scanner sc = new Scanner(System.in);
        SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy", Locale.JAPAN);
        System.out.print("Nhập vào ngày sinh của bạn: ");
        String birthDate = sc.nextLine();
        Date date = format.parse(birthDate);

        System.out.println("Ngày sinh của bạn là: " + date);
    }

    public void Question5() {
        int id = 0;
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("Tạo mới Account");
            id++;
            System.out.println("Mời bạn nhập vào userName: ");
            String userName = sc.nextLine();
            System.out.println("Mời bạn nhập vào tên đầy đủ: ");
            String fullName = sc.nextLine();
            System.out.println("Mời bạn chon phòng ban: " + "\n" + "1-Dev \n  2-Test\n  3-Scrum Master\n  4-PM");
            int pos = sc.nextInt();
            PositionName positionName;

            System.out.println("Thông tin của Accout mới được tạo là: ");
            System.out.println("id: " + id);
            System.out.println("User Name: " + userName);
            System.out.println("Full Name: " + fullName);
            switch (pos) {
                case 1:
                    System.out.println("Chức danh: " + PositionName.Dev);
                    break;
                case 2:
                    System.out.println("Chức danh: " + PositionName.Test);
                    break;
                case 3:
                    System.out.println("Chức danh: " + PositionName.Scrum_Master);
                    break;
                case 4:
                    System.out.println("Chức danh: " + PositionName.PM);
                    break;
                default:
                    System.out.println("Mời bạn nhập vào chức danh tương ứng");
                    return;
            }
            System.out.println("------------------------------------\n");
        } while (true);
    }
    public void Question6(){
        CreateDepartment();
    }
    public void CreateDepartment(){
        int id = 0;
        do {
            Scanner sc = new Scanner(System.in);
            id++;
            System.out.print("Nhập vào tên phòng ban bạn muốn tạo: ");
            String depName = sc.nextLine();

            System.out.println("Phòng ban vừa được tạo mới là: ");
            System.out.println("Department Id: " + id);
            System.out.println("Department Name: " + depName);

            System.out.println("------------------------------------\n");
        }while (true);


    }
    public void Question7(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Kiểm tra số chẵn");
        System.out.println("Mời bạn nhập vào số chẵn: ");
/*        do {
            int number = sc.nextInt();
            if (number%2!=0){
                System.out.println("Số này không phải số chẵn, mời bạn nhập số khác");
            }else System.out.println("Bạn đã nhập vào số chẵn: " + number);
            System.out.println("Bạn có muốn tiếp tục kiểm tra không ?");
            System.out.println("1- Có        2- Không");
            int check = sc.nextInt();
            switch (check){
                case 1:
                    return;
                case 2:
                    System.out.println("Mời bạn nhập vào số bất kỳ");
                    break;
            }
        }
        while (true);*/
        int numberChan = sc.nextInt();
        while (numberChan %2 !=0){
            System.out.println("Số vừa nhập không phải là số chẵn, mời bạn nhập lại");
            numberChan=sc.nextInt();
        }
        System.out.println("Số chẵn vừa nhập là: " + numberChan);
    }
}
