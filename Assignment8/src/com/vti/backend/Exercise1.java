package com.vti.backend;

import com.vti.entity.Student;

import java.util.*;

public class Exercise1 {
    public void question1(){
        Student student1 = new Student("trinh duy phuong");
        Student student2 = new Student("trinh duy phuong");
        Student student3 = new Student("trinh duy phuong");
        Student student4 = new Student("trinh duy phu");
        Student student5 = new Student("trinh duy phong");

        List<Student> students = new ArrayList<>();
        students.add(student1); // 0
        students.add(student2); // 1
        students.add(student3); // 2
        students.add(student4); // 3
        students.add(student5); // 4

        // a> In ra tổng số phần tử của students
        System.out.println("Số phần từ của students là: " + students.size());

        // b> Lấy phần tử thứ 4 của students
        System.out.println("Phần tử thứ 4 của students là: " + students.get(3));

        // c> In ra phần tử đầu vào cuối của students
        System.out.println("Phần tử đầu của students là: " + students.get(0));
        System.out.println("Phần tử cuối của students là: " + students.get(students.size()-1));

        // d> Thềm 1 phần tử vào đầu của students

        students.add(0, new Student("Nguyễn Văn A"));
        System.out.println("Phần tử đầu là: " + students.get(0));

        // d> Thềm 1 phần tử vào đầu của students
        students.add(students.size()-1, new Student("Nguyễn Văn K"));
        System.out.println("Phần tử cuối là: " + students.get(students.size()-1));

        //f> Đảo ngược vị trí của students
        Collections.reverse(students);

        //g> Tìm kiếm bằng id
        int id = 10;
        for (int i = 0 ; i<students.size(); i++){
            if (id ==students.get(i).getId()){
                System.out.println("Phần tử cần tìm: " + students.get(i));
                break;
            } if (id ==students.get(i).getId()-1){
                System.out.println("Không tìm thấy kết quả");
            }
        }

        //h> tạo 1 method tìm kiếm student theo name
         findStudent (students, "trinh duy phuong");

        //i> tìm trùng

        timtrung(students);

        // j> Copy tất cả các phần từ
        // tạo ta 1 list copy, sau đó add phần tử của list students vào















        //VD
//        Set<Student> studentHashSet = new HashSet<>();
//        studentHashSet.add(student1); // 0
//        studentHashSet.add(student2); // 1
//        studentHashSet.add(student3); // 2
//        studentHashSet.add(student1); // 0
//        studentHashSet.add(student1); // 0
//        System.out.println(studentHashSet.size());


//        System.out.println(student1);
//        System.out.println(student2);
//        System.out.println(student3);
    }

    public void timtrung(List<Student> students) {
        Set<Student> trung = new HashSet<>();
        for (int i = 0; i<students.size(); i++){
            for (int j = 1; j<students.size(); j++){
                String nameI = students.get(i).getName();
                String nameJ = students.get(j).getName();
                if (i != j && nameI.equals(nameJ)){
                    if (trung.contains(students.get(i))){
                        continue;
                    }
                    trung.add(students.get(i));
                    break;
                }
            }
        }
    }

    public void findStudent(List<Student> students, String name){
        for (int i = 0; i < students.size(); i++){
            if(students.get(i).getName().equals("trinh duy phuong")){
                System.out.println("students.get(i)");
                break;
            }
            if(i == students.size()-1){
                System.out.println("Không tìm thấy kết quả");
            }
        }
    }

    public void Question6(){
        //Để thay thế 1 object ta có thể tạo ra 1 Map tên là students có key = id
        // của student, value là name của students
        Map<Integer, String> student1= new HashMap<>();
        student1.put(1, "Nguyễn Văn A");
        student1.put(2, "Nguyễn Văn B");
        student1.put(3, "Nguyễn Văn C");
        student1.put(1, "Nguyễn Văn C");

        System.out.println(student1);

        //a In ra tất cả các key
        System.out.println(student1.keySet());

        //b In ra value
        System.out.println(student1.values());

        //add phần tử của Set vào Map

        Set<Student> studentSet = new HashSet<>();
        for(Map.Entry<Integer, String> entry : student1.entrySet()){
            Student student = new Student();
            student.setId(entry.getKey());
            student.setName(entry.getValue());
            studentSet.add(student);

        }
    }
}
