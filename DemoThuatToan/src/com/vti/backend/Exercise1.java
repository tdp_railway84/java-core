package com.vti.backend;

import com.vti.frontend.ScannerUtils;

import java.util.Arrays;
import java.util.Scanner;

public class Exercise1 {

    public void question1() {
        int[] arrays;
        int number = ScannerUtils.inputNumber(10,90);

    }

    public void question2() {
    }

    public void question3() {
    }

    public void question4() {
    }

    public void question5() {
    }

    public void question6() {
    }

    public void question7() {
        Scanner sc = new Scanner(System.in);

        String string = sc.nextLine();
        StringBuilder stringBuilder = new StringBuilder(string);
        String s2 = stringBuilder.reverse().toString();
        if(string.equals(s2)){
            System.out.println("Đây là chuỗi đối xứng");
        }else System.out.println("Đây không là chuỗi đối xứng");
    }

    public void question8(double a, double b, double c) {
        double delta = b*b - 4*a*c;
        if(delta < 0 ){
            System.out.println("Phương trình vô nghiệm");
        }else if(delta==0){
            double total = -b/(2*a);
            System.out.println("Phương trình có 2 nghiệm x1 = x2 = " + total);
        } else {
            double kq1 = (-b + Math.sqrt(delta))/2*a;
            double kq2 = (-b - Math.sqrt(delta))/2*a;
            System.out.println("Nghiệm x1 = " + kq1);
            System.out.println("Nghiệm x2 = " + kq2);
        }
    }

    public void question9() {
    }

    public void question10() {
    }

}
