package com.vti.frontend;

import com.vti.backend.Exercise1;

class A {
    int a = 5;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }
}

class B extends A {
    int a = 10;

    @Override
    public int getA() {
        return a;
    }

    @Override
    public void setA(int a) {
        this.a = a;
    }
}
public class Program {

    public static void main(String[] args) {
/*        Exercise1 exercise1 = new Exercise1();
        //Bài 1: Tìm ra 1 số lớn nhất trong 1 mảng
        exercise1.question1();
        //Bài 2: Tạo 1 method có 1 tham số int truyền vào, kiểm tra số đó có phải là số nguyên tố hay không?
        exercise1.question2();
        //Bài 3: Viết chương trình nhập vào 1 chuỗi sau đó in ra 1 chuỗi đảo ngược của nó
        exercise1.question3();
        //Bài 4: Tạo 1 method có 1 tham số int truyền vào, tính tổng các số từ 1 tới n
        exercise1.question4();
        //Bài 5: Tạo 1 method có 2 tham số truyền vào đều có kiểu dữ liệu là int, tìm ước chung lớn nhất của 2 số đó
        exercise1.question5();
        //Bài 6: Tạo 1 method có 2 tham số truyền vào đều có kiểu dữ liệu là int, tìm bội chung lớn nhất của 2 số đó
        exercise1.question6();
        //Bài 7: Tạo 1 method có 1 tham số truyền vào có kiểu dữ liệu là String, kiểm tra xem chuỗi đó có phải là chuỗi đối xứng hay không? (Chuỗi đối xứng là chuỗi mà khi đảo ngược các ký tự ta vẫn được chuỗi ban đầu)
        exercise1.question7();
        //Bài 8: Viết chương trình để giải phương trình bậc 2. Gợi ý: Phương trình bậc 2 có dạng ax2 + bx + c = 0.
        exercise1.question8(4, 5, 6);
        //Bài 9: Tạo 1 method đầu vào có 3 tham số đều có kiểu dữ liệu là double đại diện cho 3 cạnh của 1 tam giác, kiểm tra xem tam giác đó có phải là tam giác vuông hay không.
        exercise1.question9();
        //Bài 10: Tạo 1 method đầu vào là 1 số n có kiểu dữ liệu là int, tổng hợp tất cả các số là thừa số nguyên tố từ 1 tới n
        exercise1.question10();*/
        A a = new B();
        System.out.println(a);
    }

}
