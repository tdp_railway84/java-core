package com.vti.frontend;

import java.util.Scanner;

public class ScannerUtils {
    //Tạo 1 method để nhập vào màn hình 1 số từ a cho tới b
    public static int inputNumber(int min, int max) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();

        while (number < min || number > max) {
            System.out.println("Giá trị không thỏa mãn, mời bạn nhập lại");
        }

        return number;
    }


    // tạo method tạo tên
    public static String inputString() {
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
    // tạo 1 method để nhập vào 1 chuỗi có định dạng là email(có chứa @)

    // tạo ra 1 method nhập vào định dạng là 1 số điện thoại: bắt đầu là số 0

    // tạo ra 1 method nhập vào mật khẩu tùy ý có 9 đến 12 ký tự
}
